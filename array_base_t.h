#pragma once

template<class T> class array_segment_t;

template<class T>
class array_base_t
{
public:

	virtual T& at(int i) = 0;
	
	virtual int size() const = 0;

	array_segment_t<T> segment(int offset, int size);

	const T& at(int i) const {
		return (const_cast<array_base_t*>(this))->at(i);
	}

	template<class F>
	array_base_t& operator=(const array_base_t<F>& other) {
		for(int i = 0; i < size(); i++) {
			at(i) = other.at(i);
		}
		return *this;
	}

};

template<class T>
class array_segment_t : public array_base_t<T> {

public:

	virtual T& at(int i) { return base.at(i+m_offset); }
		
	virtual int size() const { return m_size; }
	
	array_segment_t(array_base_t<T>& base_, int offset_, int size_) : 
		base(base_), m_offset(offset_), m_size(size_) {}
	
	template<class F>
	array_segment_t& operator=(const array_base_t<F>& other) {
		for(int i = 0; i < size(); i++) {
			at(i) = other.at(i);
		}
		return *this;
	}
	
private:
	
	int m_size;
	int m_offset;
	array_base_t<T>& base;
};

template<class T>
array_segment_t<T> array_base_t<T>::segment(int offset, int size) {
	return array_segment_t<T>(*this, offset, size);
}

template<class T, class F>
bool operator==(const array_base_t<T>& x, const array_base_t<F>& y) {
	
	if (x.size() != y.size())
		return false;
	
	int i = 0;
		
	while (i != x.size() && x.at(i) == y.at(i)) ++i;

	return i == x.size();
}

