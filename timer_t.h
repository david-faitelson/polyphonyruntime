#pragma once

class timer_t {

public:

	bool is_data_ready(void) { return true; }
	
	bool is_after(__time64_t t) ;

	void wait_until(__time64_t t);

	__time64_t read();

};
