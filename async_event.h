#pragma once
#include "event_t.h"

class async_event_t : public event_t {
private:
	int pid;
public:

	async_event_t(int i) { pid = i; }
	
	void async_signal();
	void resume_reader();
	 
};

