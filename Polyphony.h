#pragma once

#include <limits.h>
#include "memory_layout.h"
#include "preemptive_machine.h"
#include "array.h"
#include "EEPROM_array.h"
#include "channel.h"
#include "platform.h"
#include "process.h"
#include "timer_t.h"
#include "interrupt_event.h"
#include "functor_process.h"
