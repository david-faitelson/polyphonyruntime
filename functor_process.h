#pragma once

#include "process.h"

template<typename Functor> class functor_process_t : public process_t {
	private:
		Functor functor;
	public:
		functor_process_t(const Functor& f_in) : functor(f_in) {}
		void behave() {
			functor();
		}
};

template<typename Functor> functor_process_t<Functor> functor_process(const Functor& f) { return functor_process_t<Functor>(f); }
