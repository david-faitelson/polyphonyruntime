#pragma once

class machine_process_t {
public:
  virtual void behave(void) {
    ;
  }
  virtual void terminate() {
    ;
  }
};

void machine_setup(void);

void machine_schedule(machine_process_t*); 

__attribute__((naked)) void machine_yield(void); // return control to the scheduler

__attribute__((naked)) void machine_resume_interrupt_handler(int pid);

void machine_start(void);
