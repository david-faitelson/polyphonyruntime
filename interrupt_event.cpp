#include "interrupt_event.h"
#include "platform.h"
#include "event_t.h"
#include "async_event.h"

async_event_t* isr_event[NUM_INTERRUPTS] = { 0 };

template<int n>
void polyphony_isr() {
	if (isr_event[n] != NULL) {
		isr_event[n]->async_signal();
		isr_event[n]->resume_reader();
	}
}

void (*isr_table[NUM_INTERRUPTS])(void) = {polyphony_isr<0>, polyphony_isr<1>};

void interrupt_attach(int pin, int mode, async_event_t& e) {
	int index = digitalPinToInterrupt(pin);
	isr_event[index] = &e;
	attachInterrupt(index, isr_table[index], mode);
}