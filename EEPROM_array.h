#pragma once

#include "array_base_t.h"
#include <EEPROM.h>

template<class T>
class EEPROM_cell_t {
private:
	int index;
public:
	
	EEPROM_cell_t& hold(int i) {
		index = i;
		return *this;
	}
	
	EEPROM_cell_t& operator=(const T& value) {
		EEPROM.put(index*sizeof(T), value);
		return *this;
	}
	
	operator T() const {
		T buf;
		return EEPROM.get(index*sizeof(T), buf);
		return buf;
	}
};

template<class T>
class EEPROM_array_t : public array_base_t<EEPROM_cell_t<T> > {
private: 
	EEPROM_cell_t<T> cell;
public:
  EEPROM_cell_t<T>& at(int i) { return cell.hold(i); }
  
  int size() const { return EEPROM.length(); }
};
