#pragma once
#include "event_t.h"
#include "preemptive_machine.h"

class process_t : public machine_process_t {

protected:

	event_t term_; // termination channel for this process.
	
public:

	void start();

	void join();

	void terminate();

};
