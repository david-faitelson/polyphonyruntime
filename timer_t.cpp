#include "platform.h"
#include "timer_t.h"
#include "preemptive_machine.h"

__time64_t timer_t::read()
{
	return millis();
}

bool timer_t::is_after(__time64_t t) {
	return after(millis(), t);
}

void timer_t::wait_until(__time64_t t) {

	while (!after(millis(), t))
		//machine_yield();
		;
}
