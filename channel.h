#pragma once
#include <assert.h>
#include "event_t.h"

template<class T>
class channel_t : public event_t {
private:
	T content;
public:

	T read() {
		
		wait(); 		
		
		return content;
	}

	void write(T value) {

		content = value;
		
		signal(); 
	}
	
};

