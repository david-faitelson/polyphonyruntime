#pragma once

class event_t {

protected:
	bool isDataReady;

public:

	event_t() {
		isDataReady = false;
	}

	bool is_data_ready() { return isDataReady; }

	void wait();

	void signal();
	
	void async_signal() { isDataReady = true; } // for interrupts only
};
