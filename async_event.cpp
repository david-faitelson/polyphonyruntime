#include "async_event.h"
#include "preemptive_machine.h"

void async_event_t::async_signal() {

	isDataReady = true; 		// indicate there's data 

}

void async_event_t::resume_reader() {
	machine_resume_interrupt_handler(pid);
}