#include "process.h"
#include "event_t.h"
#include "preemptive_machine.h"

#define POLYPHONY_WAIT( cond ) while(cond == false) machine_yield()

void event_t::wait() {
	
	POLYPHONY_WAIT(isDataReady);  	// wait for the data
	
	isDataReady = false; 		// indicate it has been taken
}

void event_t::signal() {
	
	isDataReady = true; 		// indicate there's data
	
	POLYPHONY_WAIT(!isDataReady); 	// wait for it to be taken
}
