#include <assert.h>
#include "process.h"

void process_t::start() {
	machine_schedule(this); 
}
 
void process_t::terminate() {
	term_.wait();
}

void process_t::join() {
	term_.signal();
}




