#pragma once

#include "array_base_t.h"

template<class T, int N>
class array_t : public array_base_t<T>
{
private:
	T element[N];
public:
	T& at(int i) { return element[i]; }
	
	int size() const { return N; }
		
	template<class F>
	array_t& atput(int i, const F& t) {
		element[i] = t; return *this;
	}
	
	template<class F>
	array_t& operator=(const array_base_t<F>& other) {
		for(int i = 0; i < N; i++) {
			element[i] = other.at(i);
		}
		return *this;
	}

};
